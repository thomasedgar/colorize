package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"path"
	"strings"
)

type Color string

const (
	ColorBlack  Color = "\u001b[30m"
	ColorRed          = "\u001b[31m"
	ColorGreen        = "\u001b[32m"
	ColorYellow       = "\u001b[33m"
	ColorBlue         = "\u001b[34m"
	ColorReset        = "\u001b[0m"
)

func colorize(color Color, regex_pattern string) {

	var line string
	var err error

	reader := bufio.NewReader(os.Stdin)

	for err == nil {
		line, err = reader.ReadString('\n')
		s := fmt.Sprintf("%s%s%s", color, regex_pattern, ColorReset)
		new_line := strings.Replace(line, regex_pattern, s, -1) // replace all (-1) occurences
		Trace.Print(new_line)
		fmt.Print(new_line)
	}

	line, err = reader.ReadString('\n')
	if err != nil {
		if err != io.EOF {
			panic(err)
		}
	}
}

var (
	Trace   *log.Logger // Just about anything
	Info    *log.Logger // Important information
	Warning *log.Logger // Be concerned
	Error   *log.Logger // Critical problem
)

func init() {
	file, err := os.OpenFile("/tmp/redden.log",
		os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	if err != nil {
		log.Printf("Failed to open log file <%s> err: <%s>", file, err)
	}

	Trace = log.New(io.MultiWriter(file, os.Stderr), "TRACE: ", log.Ldate|log.Ltime|log.Lshortfile)
	Info = log.New(file, "INFO: ", log.Ldate|log.Ltime|log.Lshortfile)
	Warning = log.New(file, "WARNING: ", log.Ldate|log.Ltime|log.Lshortfile)
	Error = log.New(io.MultiWriter(file, os.Stderr), "ERROR: ", log.Ldate|log.Ltime|log.Lshortfile)
}

//
//  MAIN
//
func main() {
	var color Color
	var r string
	var s string

	regexPtr := flag.String("regex", "", "a regular expression to colorize on")
	stringPtr := flag.String("string", "", "a regular expression to colorize on")
	flag.Parse()

	r = fmt.Sprintf("regex : <%s>\n", *regexPtr)
	Trace.Println(r)
	s = fmt.Sprintf("string: <%s>\n", *stringPtr)
	Trace.Println(s)

	this_program_name := os.Args[0]
	Trace.Println(this_program_name)
	dir, basename := path.Split(this_program_name)
	Trace.Printf("Program found in dir:<%s> basename:<%s>\n", dir, basename)

	switch basename {
	case "black":
		color = ColorBlack
	case "redden":
		color = ColorRed
	case "green":
		color = ColorGreen
	case "yellow":
		color = ColorYellow
	case "blue":
		color = ColorBlue
	default:
		color = ColorBlack
	}

	if *stringPtr != "" { // The string was supplied so replace that
		l := fmt.Sprintf("%s%s%s", color, *regexPtr, ColorReset)
		n := strings.Replace(*stringPtr, *regexPtr, l, -1) 	// replace all (-1) occurences
		Trace.Println(n)
		fmt.Println(n)
	} else { // The string was NOT supplied so read STDIN

		colorize(color, *regexPtr)
	}
}
