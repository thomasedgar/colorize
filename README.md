# README #


### What is this repository for? ###

* Colorize is a colection of color executables that will colorize patterns matching a regular expression

### What's in it for me? ###

#### Great example to run a go program from the command line ####

* uses logging and tracing
* parses command-line arguments using Flag package
* uses STDIN bufio package
* elegant in it does one thing but can be piplelined together

#### Learning opportunity ####

* Learn go and make pull requet to change code
* Fork off your own work 

### Quick Start ###

* download or clone repo
* go run green.go -regex bash < /etc/passwd (like grep, highlight - in green, the pattern 'bash')

### How do I get set up? ###

* go build -o /usr/local/bin/green green.go
* ln -s /usr/local/bin/green /usr/local/bin/blue
* ln -s /usr/local/bin/green /usr/local/bin/redden
* green --help

### Contribution guidelines ###

* grow the functionality (allow any regex expression to highlight text)
* make pull requests

### Who do I talk to? ###

* Repo owner or admin: thomas.b.edgar@gmail.com
* 1-800-PRAY-NOW

### tl;dr ###

* there is only one executable needed (same trick used in init/teleinit; yes, I'm aging myself) 
* each color can recognize itself (viz basename)
* new colors can be added such as 'black on green'
* implemted in bash (see https://bitbucket.org/thomasedgar/scripts/src/main/colors)
